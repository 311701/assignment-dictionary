global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global print_error
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_string_with_length
global read_key



section .text
 
;-> rdi-код возврата
exit: 
	mov	rax, 60			        ; Код системного вызова exit. 
	;xor rdi, rdi
   	syscall				        ; Вызов exit(rdi)


;-> rdi- адрес начала строки
;<- rax-длина строки

string_length:				
	xor	rax, rax		        ; Очистка rax
.loop:
	cmp byte [rdi+rax], 0		;Проверяем конец ли строки
	je .end_string
	inc rax				        ;увеличиваем счетчик, отвечающий за длину строкми
	jmp .loop
.end_string:				    ; если конец, то выходим
	ret
    
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
;-> rdi-адрес начала строки
print_string:
	push rdi			                ;сохраняем адрес, т к при подсчете длины строки он исчезнет
	call string_length		            ;считаем длину
	pop rdi
	mov rsi, rax			            ;записываем длину строки в аргумент для print_string_with_length
	jmp print_string_with_length	    ;печатаем строку, определенной длины
	
    

; Принимает код символа и выводит его в stdout
;-> rdi - код символа
print_char:
	push rdi;			                ;Записываем символ в стек
	mov rdi, rsp			            ;передаем на него указатель
	mov rsi, 1
	call print_string_with_length
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
;->
;<-
print_newline:
    mov rdi, 0xA		                ; Загрузили символ перевода строки
    jmp print_char		                ; Печатаем

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp
    mov r9, 10
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
.loop:
        dec rsp
        xor rdx, rdx
        div r9
        add rdx, 0x30
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
.print:
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
;->rdi- число для вывода
print_int:
    cmp rdi, 0		                        ;смотрим на знак числа
	jge .positive_number	                ; if >=0 ->positive
	je .positive_number		                ;
	neg rdi
	push rdi		                        ;сохраняем
	mov rdi, 45		                        ; -
	call print_char		                    ;печатаем -
	pop rdi			                        ; восстанавливаем число
.positive_number:
	call print_uint			                ;печатаем число без знака
	ret


;-> rdi- указатель на первую строку, rsi- указатель на вторую строку
;rax <- 1, если строки равны
;rax <- 0, если строки не равны 
string_equals:
   	xor rax, rax		
.loop:
    	mov r8b, [rdi]		                ; символ первой строки
    	mov r9b, [rsi]		                ; символ второй строки
    	cmp r8b, r9b		                ; сравниваем символы
	jne .not_equals		                    ; переход, если не равны
	inc rdi			                        ; увеличиваем адрес символа в 1 строке
	inc rsi			                        ; увеличиваем адрес символа во 2 строке
	cmp r8b, 0		                        ; проверяем конец ли строки
	jne .loop
	mov rax,1
.not_equals:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    	xor rax, rax		                ;номер системного вызова
	push rax		                        ;место в стеке для символа, который считаем
	mov rdi, 0		                        ;поток ввода
	mov rsi, rsp		                    ;куда запишется символ
	mov rdx, 1		                        ;количество считанных символов
	syscall			                        ;системный вызов
	pop rax
	ret 			                        ;возврат из функции




read_key:

.tabs:
	xor rax, rax		; очистка 
	xor rdx, rdx		; дальше будет страшно :o
	push rdi
	push rsi               	;  . .
	push rdx		    ;  ---
	call read_char
	pop rdx
	pop rsi
	pop rdi
	test rsi, rsi		;Длина буфера 0?
	je .err			    ;0 -> .err
	test rax, rax		; конец слова?
 	je .end			    ;конец -> .end
	;cmp rax,0x20		;Пробельный символ?
	;je .tabs		    ;ДА -> .tabs
	cmp rax,0x9
	je .tabs
	cmp rax,0xA
	je .tabs
	
.write:
	mov byte[rdi + rdx], al	    ; Записываем считанный символ в соответствующую ячейку в буфере
  	inc rdx			            ; Увеличение длины считанного слова на 1
    	cmp rdx, rsi		    ; сравниваем длину с буфером
	je .err		        	    ; если места нет, уходим

	
				                ; Дальше просто читаем все символы и записываем в буфер
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp rax, 0		    ; конец слова?
 	je .end			    ;конец -> .end
	;cmp rax, 0x20		; пробел
    ;je .end			
    cmp rax, 0x9		; табуляция
    je .end			
    cmp rax, 0xA		; перевода строки
    je .end
	jmp .write		    ;записывем букву и пытаемся считать новую

	

.err:
	xor rax, rax		    ; При ошибке очищаем регистры,
    	xor rdx, rdx		; которые возвращаем
    	ret
.end:
	mov byte[rdi + rdx], 0	; Ситуация успеха
    	mov rax, rdi    	; Записываем в rax указатель на буфер
    	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор



;-> rdi- начало буфера
;-> rsi- длина буфера
;rax <- адрес буфера при успехе
;rax <- 0 при неудаче
;rdx <- длина буфера при успехе
read_word:
.tabs:
	xor rax, rax		; очистка 
	xor rdx, rdx		; дальше будет страшно :o
	push rdi
	push rsi               	;  . .
	push rdx		    ;  ---
	call read_char
	pop rdx
	pop rsi
	pop rdi
	test rsi, rsi		;Длина буфера 0?
	je .err			    ;0 -> .err
	test rax, rax		; конец слова?
 	je .end			    ;конец -> .end
	;cmp rax,0x20		;Пробельный символ?
	;je .tabs		    ;ДА -> .tabs
	cmp rax,0x9
	je .tabs
	cmp rax,0xA
	je .tabs
	
.write:
	mov byte[rdi + rdx], al	    ; Записываем считанный символ в соответствующую ячейку в буфере
  	inc rdx			            ; Увеличение длины считанного слова на 1
    	cmp rdx, rsi		    ; сравниваем длину с буфером
	je .err		        	    ; если места нет, уходим

	
				                ; Дальше просто читаем все символы и записываем в буфер
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp rax, 0		    ; конец слова?
 	je .end			    ;конец -> .end
	cmp rax, 0x20		; пробел
    je .end			
    cmp rax, 0x9		; табуляция
    je .end			
    cmp rax, 0xA		; перевода строки
    je .end
	jmp .write		    ;записывем букву и пытаемся считать новую

	

.err:
	xor rax, rax		    ; При ошибке очищаем регистры,
    	xor rdx, rdx		; которые возвращаем
    	ret
.end:
	mov byte[rdi + rdx], 0	; Ситуация успеха
    	mov rax, rdi    	; Записываем в rax указатель на буфер
    	ret	


; Принимает указатель на строку rdi, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rcx, rcx
	xor r9,r9
	mov r8, 10 			        ;для сдвига числа на разряд
	mov r9b, byte[rdi+rcx] 		;считали первый символ
	sub r9b, '0' 			; получили число
	jl .err 			    ;если <0 -> err
	cmp r9b, 9 			    ;
	ja .err				    ;если >9 -> err
	add rax, r9
	inc rcx
	test rax, rax 			;проверка на 0 первого числа
	je .end
.loop:
	mov r9b, byte[rdi+rcx] 		;считали следующий символ
	sub r9b, '0' 			    ; получили число
	jl .end 			        ;если <0 -> end
	cmp r9b, 9 			        ;
	ja .end 			        ;если >9 -> end
	mul r8 				        ; сдвигаем число на разряд
	add rax, r9 			    ; добавляем считанную цифру
	inc rcx
	jmp .loop
.err:
	xor rdx, rdx
	ret
.end:
	mov rdx, rcx
	ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.

;->rdi- указатель на строку
;rax <- число
;rdx <- длина в символах
;rdx <- 0, если не удалось считать
parse_int:
	mov r8b, byte[rdi]	   ;считали первый символ
	cmp r8b, '-'		    ;if -
	je .minus		        ;->minus
	mov r10, 1		        ;в r10 указатель на +(1) или -(0)
	cmp r8b,'+'
	je .plus
	jmp .number
.minus:
	mov r10,0
.plus:
	inc rdi			    ; если первый символ- знак
.number:
	call parse_uint		;считываем число без знака
	test r10, r10		;if r10==1
	jne .end		    ;->end
	neg rax			    ;else -rax
	inc rdx			    ;увеличиваем длину в символах
.end:
	ret
	
;указатель на строку-> rdi
;указатель на буфер-> rsi 
;длина буфера-> rdx 
;rax <- длина строки, если уместилась(иначе 0)
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    	xor rax, rax		        ;обнуляем rax
.loop:
	cmp rax,rdx		                ;сравниваем длину, скопированной строки, и длину буфера
	je .err			                ;строка>буфера-> error
	mov r8b, [rdi+rax]	            ;копируем элемент в буфер
	mov byte[rsi+rax], r8b	        ;
	inc rax			                ;rax++
	test byte[rsi+rax], 0xff	    ;конец строки?
	je .end			                ;конец-> end
	jmp .loop		                ;идем копировать следующий элемент
.err:
	mov rax, 0
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
		push rdi			
		mov rsi, rdi 		
		call string_length	
		pop rsi			
		mov rdx, rax		
		mov rax, 1			
		mov rdi, 2			
		syscall
		ret


;-> rsi- длина строки
;-> rdi- адрес начала
print_string_with_length:	;rsi содержит длину строки , rdi-адрес начала
	mov rdx, rsi		    ;длина строки
	mov rsi, rdi		    ;начало строки
	mov rax, 1	        	;номер системного вызова
	mov rdi, 1		        ;поток вывода
	syscall			        ;системный вызов
	ret			            ;выход из подпрограммы
