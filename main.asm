%include "lib.inc"
%include "colon.inc"


extern find_word
global _start


section .rodata
%include "words.inc"

input_text: db 'Введиле ключ для поиска: ', 0
error_input: db 'У ключа неверное значение (слишком длинный)', 0
error_find: db 'По данному ключу ничего не было найдено', 0

section .bss
input_buffer: resb 256



global _start


section .text

_start:
	mov rdi, input_text
	call print_error		;-> Введите ключ для поиска: ->stderr
	mov rsi, 256
	mov rdi,  input_buffer
	call read_key			;<-Считали ключ в буфер
	test rax, rax
	je .error_input			;переходим, если неверное значение у ключа
	
	mov rdi, input_buffer		;указатель на ключ -> rdi
	mov rsi, elem			;указатель на словарь->rsi
	call find_word			;ищем слово
	test rax, rax
	je .error_find			;переходим, если ничего не нашли
	mov rdi, rax	
   	add rdi, 8
    	push rdi
    	call string_length		
    	pop rdi
    	inc rdi
    	add rdi, rax
    	call print_string		;->выводим
	call exit

.error_input:
	mov rdi, error_input
	jmp .end
.error_find:
	mov rdi, error_find
.end:
	call print_error
	call exit
	
